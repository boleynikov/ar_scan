﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;
using TMPro;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System;

public class QRScanner : MonoBehaviour
{
    [SerializeField] GameObject questionBtn;
    [SerializeField] GameObject focusPanel;
    [SerializeField] TextMeshProUGUI debugText;
    [SerializeField] ARCameraManager cameraManager;
    private HashSet<string> codeTexts;
    private CodeAnalysis analysis;
    private CodeType.Kind codeType;
    private string scanResult;
    private int i = 0;
    private bool startScan = false;
    private RenderTexture rt;
    private Texture2D shot;
    private void Start()
    {
        codeTexts = new HashSet<string>();
        Camera.main.clearFlags = CameraClearFlags.SolidColor;
    }
    /// <summary>
    /// OkButton Press method
    /// </summary>
    public void StartScan()
    {
        startScan = true;
        StartCoroutine(Scan());
    }
    /// <summary>
    /// Scanning method
    /// </summary>
    /// <returns></returns>
    IEnumerator Scan()
    {
        while (startScan) 
        {
            yield return new WaitForSecondsRealtime(2f); // we leave every two seconds to avoid brakes

            rt = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGB32); // assigning render texture for camera
            RenderTexture.active = rt;
            Camera.main.targetTexture = rt;
            shot = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false); // texture for screenshoot
            yield return new WaitForEndOfFrame();
            Camera.main.Render();
            RenderTexture temp = RenderTexture.GetTemporary(Camera.main.activeTexture.descriptor); 
            shot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false); // push screendata to shot texture
            shot.Apply();
            Camera.main.targetTexture = null;
            RenderTexture.active = null;
            Destroy(rt);
            RenderTexture.ReleaseTemporary(temp);
            IBarcodeReader barcodeReader = new BarcodeReader();
            var result = barcodeReader.Decode(shot.GetRawTextureData(), shot.width, shot.height, RGBLuminanceSource.BitmapFormat.ARGB32); // decoding info from screenshoot
            if (result != null)
            {
                var check = codeTexts.Contains(result.Text); 
                if (!check) // adding image only if we haven't it already
                {
                    codeType = CodeType.Kind.QR;
                    if (result.BarcodeFormat.Equals(BarcodeFormat.QR_CODE)) // encoding object is QR code
                    {                       
                        i++;
                        codeTexts.Add(result.Text);
                        debugText.text = "You are trying to read Qr " + i;
                        scanResult = result.Text;
                        ImageLibraryBehaviour.library.AddImage(shot, analysis, codeType, scanResult);
                    }
                    else if (result.BarcodeFormat.Equals(BarcodeFormat.EAN_13) && result.Text.Length >= 12) // encoding object is Bar code
                    {
                        i++;
                        codeTexts.Add(result.Text);
                        codeType = CodeType.Kind.Bar;
                        analysis = new CodeAnalysis(result.Text);
                        debugText.text = "You are trying to read Bar " + i;
                        scanResult = result.Text;
                        ImageLibraryBehaviour.library.AddImage(shot, analysis, codeType, scanResult);
                    }
                }
                if (shot != null)
                    Destroy(shot);
            }
            if (shot != null) Destroy(shot);
        }
        
    }
    /// <summary>
    /// Generate QrCode for displaying an icon on tooltip
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static Texture GenerateQR(string text)
    {       
        Debug.Log("String convert: " + text);
        var encoded = new Texture2D(256, 256);
        var colors32 = Encode(text);
        encoded.SetPixels32(colors32);
        encoded.Apply();
        
        return encoded;
    }
   /// <summary>
   /// Encoding text to Color32[] type
   /// </summary>
   /// <param name="text"></param>
   /// <returns></returns>
    private static Color32[] Encode(string text)
    {
        var writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = 256,
                Width = 256
            }
        };
        return writer.Write(text);
    }
    /// <summary>
    /// pressing the button to show the UI focus element 
    /// </summary>
    public void HidePanel()
    {
        focusPanel.SetActive(!focusPanel.activeSelf);
    }
}
