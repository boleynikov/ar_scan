﻿using System.Collections;
using System.Drawing;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using UnityEngine.UI;

public class ImageLibraryBehaviour : MonoBehaviour
{
    public static ImageLibraryBehaviour library;

    [SerializeField] TextMeshProUGUI trackedImageText;
    [SerializeField] ARTrackedImageManager manager;
    [SerializeField] GameObject mTrackedImagePrefab;
    private MutableRuntimeReferenceImageLibrary myRuntimeReferenceImageLibrary;
    private Dictionary<string, GameObject> arObjects = new Dictionary<string, GameObject>();
    private string legalTxt = "product is legal";
    private string ilegalTxt = "product is ilegal";
    private bool CheckInfoBtnPressed = false;
    /// <summary>
    /// Creating runtime reference library and it's mutableRuntimeReferenceLibrary for adding images
    /// </summary>
    void Awake()
    {
        library = this;
        manager.referenceLibrary = manager.CreateRuntimeLibrary();
        manager.requestedMaxNumberOfMovingImages = 1;
        myRuntimeReferenceImageLibrary = manager.referenceLibrary as MutableRuntimeReferenceImageLibrary;
    }
    private void OnEnable()
    {
        manager.trackedImagesChanged += OnTrackedImagesChanged;
    }
    private void OnDisable()
    {
        manager.trackedImagesChanged -= OnTrackedImagesChanged;
    }
    /// <summary>
    /// Adding image to runtime reference library and creating tooltip prefab
    /// </summary>
    /// <param name="texture"></param>
    /// <param name="analysis"></param>
    /// <param name="Type"></param>
    /// <param name="txt"></param>
    public void AddImage(Texture2D texture, CodeAnalysis analysis, CodeType.Kind Type, string txt)
    {
        var imageName = Guid.NewGuid();
        var job = myRuntimeReferenceImageLibrary.ScheduleAddImageWithValidationJob(texture, imageName.ToString(), 0.05f);      
        GameObject newObject = Instantiate(mTrackedImagePrefab, Vector3.zero, Quaternion.identity);
        newObject.transform.localScale = new Vector3(0.005f, 0.005f, 0.005f);
        newObject.transform.Rotate(90, 180, 0);
        newObject.name = imageName.ToString();
        var toolTip = newObject.GetComponent<TextHandler>();
        toolTip.kind = Type;
        if (toolTip.kind.Equals(CodeType.Kind.Bar))
        {
            toolTip.Title.text = "Bar Code";
            toolTip.legal = analysis.CheckInfo() ? true : false;           
            toolTip.country = analysis.Country();           
            toolTip.manufactureCode = analysis.manufactureCode;
            toolTip.productCode = analysis.productCode;
            UpdateToolTipText(toolTip);
        }
        else if (toolTip.kind.Equals(CodeType.Kind.QR))
        {
            try
            {
                toolTip.panel.GetComponent<MeshRenderer>().material.mainTexture = QRScanner.GenerateQR(txt);
                Debug.Log("Texture added");
            }
            catch(Exception e)
            {
                Debug.Log(e.Message);
            }
            toolTip.qrCodeTxt = txt;
            toolTip.Title.text = "Qr Code";
            toolTip.label.text = toolTip.qrCodeTxt;
        }        
        arObjects.Add(newObject.name, newObject);
        manager.enabled = true;
    }
    /// <summary>
    /// Detecting and tracking images from reference library 
    /// </summary>
    /// <param name="eventArgs"></param>
    private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage trackedImage in eventArgs.added) // Detecting images
        {
            trackedImageText.text = $"Added {trackedImage.referenceImage.name} type {arObjects[trackedImage.referenceImage.name].GetComponent<TextHandler>().kind}"; 
            UpdateDetails(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.updated) // Tracking images
        {
            foreach (GameObject go in arObjects.Values)
            {
                if (go.name != trackedImage.referenceImage.name)
                    go.SetActive(false);
            }
            trackedImageText.text = $"Tracked {trackedImage.referenceImage.name} type {arObjects[trackedImage.referenceImage.name].GetComponent<TextHandler>().kind}";
            UpdateDetails(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            trackedImageText.text = $"Removed {trackedImage.referenceImage.name} type {arObjects[trackedImage.referenceImage.name].GetComponent<TextHandler>().kind}";
            arObjects[trackedImage.referenceImage.name].SetActive(false);
        }
    }
    /// <summary>
    /// Update tooltip parameters
    /// </summary>
    /// <param name="trackedImage"></param>
    void UpdateDetails(ARTrackedImage trackedImage)
    {
        GameObject prefab = arObjects[trackedImage.referenceImage.name];
        var tooltip = prefab.GetComponent<TextHandler>();
        prefab.transform.position = trackedImage.transform.position;
        tooltip.lineRenderer.SetPosition(0, trackedImage.transform.position);
        tooltip.lineRenderer.SetPosition(1, tooltip.label.transform.position);
        prefab.SetActive(true);
        UpdateToolTipText(tooltip);
    }
    /// <summary>
    /// Update text labels of tooltip
    /// </summary>
    /// <param name="toolTip"></param>
    private void UpdateToolTipText(TextHandler toolTip)
    {
        if (toolTip.kind.Equals(CodeType.Kind.Bar))
        {
            if (toolTip.legal) // product is legal
            {
                toolTip.label.text = legalTxt + "\n";
                if (CheckInfoBtnPressed) // showing detail info
                {
                    toolTip.label.text =
                    "Country: " + toolTip.country + "\n" +
                    "Manufacture: " + toolTip.manufactureCode + "\n" +
                    "Product code: " + toolTip.productCode;
                }
                else
                    toolTip.label.text = legalTxt;
            }
            else // product is not legal
            {
                toolTip.label.text = ilegalTxt + "\n";
                if (CheckInfoBtnPressed) 
                {
                    toolTip.label.text =
                    "Country: " + toolTip.country + "\n" +
                    "Manufacture: " + toolTip.manufactureCode + "\n" +
                    "Product code: " + toolTip.productCode;
                }
                else
                    toolTip.label.text = ilegalTxt + "\n";
            }
        }
        else if (toolTip.kind.Equals(CodeType.Kind.QR))
        {
            toolTip.label.text = toolTip.qrCodeTxt;
        }
    }
    /// <summary>
    /// ShowDetails button press method
    /// </summary>
    public void CheckInfoBtn()
    {
        CheckInfoBtnPressed = !CheckInfoBtnPressed;
    }
}