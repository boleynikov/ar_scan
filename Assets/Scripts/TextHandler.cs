using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextHandler : MonoBehaviour
{
    public CodeType.Kind kind; // kind of code: QR or Bar

    public GameObject panel;
    public TextMeshPro Title;
    public TextMeshPro label;
    // Qrcode info
    public string qrCodeTxt = "";
    // Barcode info
    public bool legal = false;
    public string country = "";
    public string manufactureCode = "";
    public string productCode = "";

    public LineRenderer lineRenderer;
    private void Start()
    {
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
        lineRenderer.SetWidth(0.0005f, 0.0005f);
    }
}
