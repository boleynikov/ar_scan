﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryIdentifier : MonoBehaviour
{
    //Index - Country dictionary
    private Dictionary<string, string> countries = new Dictionary<string, string>() { };
    /// <summary>
    /// Getting Country name by it's index
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public string GetByIndex(string index) => countries[index];
    /// <summary>
    /// Filling the dictionary inside the constructor
    /// </summary>
    public CountryIdentifier()
    {
        for (int i = 0; i < 10; i++)
            countries.Add("0" + i.ToString(), "USA and Canada");
        for (int i = 30; i < 38; i++)
            countries.Add(i.ToString(), "France");
        countries.Add("49", "Japan");
        countries.Add("50", "Great Britain");
        countries.Add("54", "Belgium and Luxembourg");
        countries.Add("64", "Finland");
        countries.Add("70", "Norway");
        countries.Add("73", "Sweden");
        countries.Add("76", "Switzerland");
        countries.Add("80", "Italy");
        countries.Add("81", "Italy");
        countries.Add("82", "Italy");
        countries.Add("83", "Italy");
        countries.Add("84", "Spain");
        countries.Add("87", "Netherlands");
        countries.Add("90", "Austria");
        countries.Add("91", "Austria");
        countries.Add("93", "Australia");
        countries.Add("94", "New Zealand");
        for (int i = 140; i < 200; i++)
            countries.Add(i.ToString(), "Russia (internal)");
        for (int i = 200; i < 300; i++)
            countries.Add(i.ToString(), "For limited use");
        countries.Add("380", "Bulgaria");
        countries.Add("383", "Slovenia");
        countries.Add("385", "Croatia");
        countries.Add("389", "Montenegro");
        for (int i = 400; i < 441; i++)
            countries.Add(i.ToString(), "Germany");       
        countries.Add("460", "Russia");
        countries.Add("461", "Russia");
        countries.Add("462", "Russia");
        countries.Add("463", "Russia");
        countries.Add("464", "Russia");
        countries.Add("465", "Russia");
        countries.Add("466", "Russia");
        countries.Add("467", "Russia");
        countries.Add("468", "Russia");
        countries.Add("469", "Russia");
        countries.Add("470", "Kyrgyzstan");
        countries.Add("471", "Taiwan");
        countries.Add("474", "Estonia");
        countries.Add("475", "Latvia");
        countries.Add("476", "Azerbaijan");
        countries.Add("477", "Lithuania");
        countries.Add("478", "Uzbekistan");
        countries.Add("479", "Sri Lanka");
        countries.Add("480", "Philippines");
        countries.Add("481", "Belarus");
        countries.Add("482", "Ukraine");
        countries.Add("483", "Turkmenistan");
        countries.Add("484", "Moldova");
        countries.Add("485", "Armenia");
        countries.Add("486", "Georgia");
        countries.Add("487", "Kazakhstan");
        countries.Add("488", "Tajikistan");
        countries.Add("489", "Hong Kong");   
        countries.Add("520", "Greece");
        countries.Add("521", "Greece");
        countries.Add("528", "Lebanon");
        countries.Add("529", "Cyprus");
        countries.Add("530", "Albania");
        countries.Add("531", "Macedonia");
        countries.Add("535", "Malta");
        countries.Add("539", "Ireland");      
        countries.Add("560", "Portugal");
        countries.Add("569", "Iceland");
        countries.Add("570", "Denmark"); 
        countries.Add("571", "Denmark");
        countries.Add("572", "Denmark");
        countries.Add("573", "Denmark");
        countries.Add("574", "Denmark");
        countries.Add("575", "Denmark");
        countries.Add("576", "Denmark");
        countries.Add("577", "Denmark");
        countries.Add("578", "Denmark");
        countries.Add("579", "Denmark");
        countries.Add("590", "Poland");
        countries.Add("594", "Romania");
        countries.Add("599", "Hungary");
        countries.Add("600", "South Africa");
        countries.Add("601", "South Africa");
        countries.Add("603", "Ghana");
        countries.Add("604", "Senegal");
        countries.Add("608", "Bahrain");
        countries.Add("609", "Mauritius");
        countries.Add("611", "Morocco");
        countries.Add("613", "Algeria");
        countries.Add("615", "Nigeria");
        countries.Add("616", "Kenya");
        countries.Add("618", "Ivory Coast");
        countries.Add("619", "Tunisia");
        countries.Add("620", "Tanzania");
        countries.Add("621", "Syria");
        countries.Add("622", "Egypt");
        countries.Add("623", "Brunei");
        countries.Add("624", "Libya");
        countries.Add("625", "Jordan");
        countries.Add("626", "Iran");
        countries.Add("627", "Kuwait");
        countries.Add("628", "Saudi Arabia");
        countries.Add("629", "Emirates");
        countries.Add("687", "Bosnia and Herzegovina");
        countries.Add("690", "China");
        countries.Add("691", "China");
        countries.Add("692", "China");
        countries.Add("693", "China");
        countries.Add("694", "China");
        countries.Add("695", "China");
        countries.Add("696", "China");
        countries.Add("697", "China");
        countries.Add("698", "China");
        countries.Add("699", "China");        
        countries.Add("729", "Israel");        
        countries.Add("740", "Guatemala");
        countries.Add("741", "Salvador");
        countries.Add("742", "Honduras");
        countries.Add("743", "Nicaragua");
        countries.Add("744", "Costa Rica");
        countries.Add("745", "Panama");
        countries.Add("746", "Dominican Republic");
        countries.Add("750", "Mexico");        
        countries.Add("759", "Venezuela");        
        countries.Add("770", "Colombia");
        countries.Add("771", "Colombia");
        countries.Add("773", "Uruguay");
        countries.Add("775", "Peru");
        countries.Add("777", "Bolivia");
        countries.Add("779", "Argentina");
        countries.Add("780", "Chile");
        countries.Add("784", "Paraguay");
        countries.Add("786", "Ecuador");
        countries.Add("789", "Brazil");
        countries.Add("790", "Brazil");   
        countries.Add("850", "Cuba");
        countries.Add("858", "Slovakia");
        countries.Add("859", "Czech Republic");
        countries.Add("860", "Serbia");
        countries.Add("865", "Mongolia");
        countries.Add("867", "North Korea");
        countries.Add("868", "Turkey");
        countries.Add("869", "Turkey");       
        countries.Add("880", "South Korea");
        countries.Add("884", "Cambodia");
        countries.Add("885", "Thailand");
        countries.Add("888", "Singapore");
        countries.Add("890", "India");
        countries.Add("893", "Vietnam");
        countries.Add("896", "Pakistan");
        countries.Add("899", "Indonesia");
        countries.Add("950", "GS1 Global Office");
        countries.Add("951", "Global Office — General Manager Number");
        countries.Add("955", "Malaysia");
        countries.Add("958", "Macau");
        for (int i = 960; i < 970; i++)
        {
            countries.Add(i.ToString(), "Global Office — GTIN-8");
        }
        countries.Add("977", "International periodicals ISSN");
        countries.Add("978", "International book production Bookland ISBN");
        countries.Add("979", "International periodicals ISSN");
        countries.Add("980", "International refund");
        countries.Add("981", "International coupon identification for standard currencies");
        countries.Add("982", "International coupon identification for standard currencies");
        countries.Add("983", "International coupon identification for standard currencies");
        countries.Add("984", "International coupon identification for standard currencies");
    }
}
