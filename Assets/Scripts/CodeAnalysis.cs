﻿using UnityEngine;

public class CodeAnalysis : MonoBehaviour
{
    public string countryCode;
    public string manufactureCode;
    public string productCode;
    private string checkSum;
    private int rest;
    private CountryIdentifier identificate = new CountryIdentifier(); // country identifier instance for getting Country name
    /// <summary>
    /// Barcode analyzer constructor 
    /// inside which we calculate the rest for legality
    /// </summary>
    /// <param name="barCode"></param>
    public CodeAnalysis(string barCode)
    {
        int firstSum = 0;
        int secondSum = 0;
        Debug.Log("Barcode is " + barCode + " L " + barCode.Length);
        countryCode = barCode.Substring(0, 3);
        manufactureCode = barCode.Substring(3, 4);
        productCode = barCode.Substring(7, 5);
        checkSum = barCode.Substring(barCode.Length - 1, 1);
        if (barCode.Length == 12)
        {
            countryCode = barCode.Substring(0, 2);
            manufactureCode = barCode.Substring(2, 4);
            productCode = barCode.Substring(6, 5);
            firstSum -= int.Parse(checkSum);
        }
        for (int i = 0; i < barCode.Length - 1; i+=2)
        {                                                                                                                                                                                                                                                                                                                                                                                     
            firstSum += int.Parse(barCode[i + 1].ToString());
            secondSum += int.Parse(barCode[i].ToString());
        }
        Debug.Log("first sum is " + firstSum);
        Debug.Log("second sum is " + secondSum);
        int tmp = (firstSum * 3) + secondSum;
        rest = tmp % 10;
        Debug.Log("Rest is " + rest);
    }
    /// <summary>
    /// Сhecking if the product is legal by the last character of the barcode
    /// </summary>
    /// <returns></returns>
    public bool CheckInfo() => ((10 - rest) == int.Parse(checkSum));
    /// <summary>
    /// Getting Country
    /// </summary>
    /// <returns></returns>
    public string Country() => identificate.GetByIndex(countryCode);
}
